'use strict';
import {createStackNavigator} from 'react-navigation';
import {VenueMapScreen} from '../features/map';
import styles from './styles';

const routeConfig = {
  'VenueMap': {
    screen: VenueMapScreen,
    headerBackTitleVisible: true,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Venue Map', // %TODO%
    }),
  },
};

const stackNavigatorConfig = {
  initialRouteName: 'VenueMap',
};

const MapStack = createStackNavigator(routeConfig, stackNavigatorConfig);

export default MapStack;
