'use strict';
/* eslint-disable react/display-name */
import {Button, Icon} from 'native-base';
import React from 'react';
import {createDrawerNavigator, createStackNavigator} from 'react-navigation';
import {PublicationScreen} from '../features/publication';
import {SponsorScreen} from '../features/sponsor';
import {LegalScreen} from '../features/legalnotice';
import {PrivacyScreen} from '../features/privacypolicy';
import {AuthorListScreen} from '../features/author';
import {AuthorDetailScreen} from '../features/author';
import {ContactScreen} from '../features/contact';
import {SubmissionDetailScreen} from '../features/submission';
import HomeStack from './HomeStack';
import styles from './styles';

const routeConfig = {
  'Main Menu': {
    screen: HomeStack,
    navigationOptions: ({navigation}) => ({
      drawerIcon: () => <Icon name="home" type="SimpleLineIcons"
        style={styles.drawerIcon} />,
      headerStyle: styles.navbarHeader,
      title: 'Main Menu', // %TODO%
    }),
  },
  'Authors': {
    screen: createStackNavigator({
      'AuthorListScreen': {
        screen: AuthorListScreen,
        navigationOptions: ({navigation}) => ({
          headerStyle: styles.navbarHeader,
          title: 'Authors', // %TODO%
          headerLeft: (
            <Button transparent onPress={() => navigation.toggleDrawer()}>
              <Icon name="md-menu" type="Ionicons" />
            </Button>
          ),
        }),
      },
      'AuthorDetailScreen': {
        screen: AuthorDetailScreen,
        navigationOptions: ({navigation}) => ({
          headerStyle: styles.navbarHeader,
          title: 'Author Details',  // %TODO%
        }),
      },
      'AuthorSubmissionDetailScreen': {
        screen: SubmissionDetailScreen,
        navigationOptions: ({navigation}) => ({
          headerStyle: styles.navbarHeader,
          title: 'Paper Detail',  // %TODO%
        }),
      },
    }),
    navigationOptions: ({navigation}) => ({
      drawerIcon: () => <Icon name="people" type="SimpleLineIcons"
        style={styles.drawerIcon} />,
    }),
  },
  'Publication': {
    screen: PublicationScreen,
    navigationOptions: ({navigation}) => ({
      drawerIcon: () => <Icon name="book-open" type="SimpleLineIcons"
        style={styles.drawerIcon} />,
      title: 'Publication', // %TODO%
    }),
  },
  'LegalNotice': {
    screen: LegalScreen,
    navigationOptions: ({navigation}) => ({
      drawerIcon: () => <Icon name="info" type="SimpleLineIcons"
        style={styles.drawerIcon} />,
      title: 'Legal Notice',  // %TODO%
    }),
  },
  'Sponsor': {
    screen: SponsorScreen,
    navigationOptions: ({navigation}) => ({
      drawerIcon: () => <Icon name="grid" type="SimpleLineIcons"
        style={styles.drawerIcon} />,
      title: 'Sponsor', // %TODO%
    }),
  },
  'Contact': {
    screen: ContactScreen,
    navigationOptions: ({navigation}) => ({
      drawerIcon: () => <Icon name="earphones-alt" type="SimpleLineIcons"
        style={styles.drawerIcon} />,
      title: 'Contact Us',  // %TODO%
    }),
  },
  'PrivacyPolicy': {
    screen: PrivacyScreen,
    navigationOptions: ({navigation}) => ({
      drawerIcon: () => <Icon name="lock" type="SimpleLineIcons"
        style={styles.drawerIcon} />,
      title: 'Privacy Policy',  // %TODO%
    }),
  },
};

const drawerNavigatorConfig = {
  drawerBackgroundColor: '#3C404B',
  initialRouteName: 'Main Menu',
  contentOptions: {
    labelStyle: styles.drawerLabelStyle,
  },
};

const HomeDrawerNavigator = createDrawerNavigator(routeConfig,
    drawerNavigatorConfig);

export default HomeDrawerNavigator;
