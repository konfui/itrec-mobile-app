'use strict';
import {Button, Icon} from 'native-base';
import React from 'react';
import {createStackNavigator} from 'react-navigation';
import {AuthorDetailScreen} from '../features/author';
import {EventDetailScreen, EventListScreen} from '../features/events';
import {MainMenuScreen} from '../features/mainmenu';
import {NewsDetailScreen} from '../features/news';
import {PlenaryDetailScreen, PlenaryListScreen} from '../features/plenary';
import {SearchResultScreen} from '../features/search';
import {SubmissionDetailScreen,
  SubmissionListScreen} from '../features/submission';
import {SymposiaListScreen} from '../features/symposia';
import styles from './styles';

const routeConfig = {
  'MainMenuScreen': {
    screen: MainMenuScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'i-TREC',
      headerLeft: (
        <Button transparent onPress={() => navigation.toggleDrawer()}>
          <Icon name="md-menu" type="Ionicons" />
        </Button>
      ),
    }),
  },
  'NewsDetailScreen': {
    screen: NewsDetailScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'News',  // %TODO%
    }),
  },
  'EventRundown': {
    screen: EventListScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Event Rundown', // %TODO%
    }),
  },
  'EventDetail': {
    screen: EventDetailScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Event Details', // %TODO
    }),
  },
  'SymposiaList': {
    screen: SymposiaListScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Parallel Sessions', // %TODO%
    }),
  },
  'SubmissionList': {
    screen: SubmissionListScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Submitted Papers',  // %TODO%
    }),
  },
  'SearchResultScreen': {
    screen: SearchResultScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Search Results',  // %TODO%
    }),
  },
  'AuthorSearchResultScreen': {
    screen: AuthorDetailScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Author Details',  // %TODO%
    }),
  },
  'SubmissionDetailScreen': {
    screen: SubmissionDetailScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Paper Detail',  // %TODO%
    }),
  },
  'PlenaryListScreen': {
    screen: PlenaryListScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Keynote/Invited Speakers',  // %TODO%
    }),
  },
  'PlenaryDetailScreen': {
    screen: PlenaryDetailScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Session Details', // %TODO%
    }),
  },

};

const stackNavigatorConfig = {
  initialRouteName: 'MainMenuScreen',
};

const HomeStack = createStackNavigator(routeConfig, stackNavigatorConfig);

export default HomeStack;
