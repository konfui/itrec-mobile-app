'use strict';
import {createStackNavigator} from 'react-navigation';
import MessageScreen from '../features/message/MessageScreen';
import styles from './styles';

const routeConfig = {
  'MessageScreen': {
    screen: MessageScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Chat',  // %TODO%
    }),
  },
};

const stackNavigatorConfig = {
  initialRouteName: 'MessageScreen',
};

const MessageStack = createStackNavigator(routeConfig, stackNavigatorConfig);

export default MessageStack;
