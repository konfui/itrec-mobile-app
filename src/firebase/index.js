// SPDX-License-Identifier: GPL-3.0-or-later
'use strict';
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';

const CONFIG = {
  apiKey: 'AIzaSyBWzGae8NuNQXxM14huxw6M7o4X7RQWG2c',
  projectId: 'i-trec',
  storageBucket: 'i-trec.appspot.com',
};

const app = firebase.initializeApp(CONFIG);
const firestore = app.firestore();
const storage = app.storage();

export {firestore, storage};
