'use strict';
import React from 'react';
import {FlatList, View, Text} from 'react-native';
import MessageListItem from './MessageListItem';
import styles from './styles';
import PropTypes from 'prop-types';

class MessageList extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  scrollToBottom() {
    // this.ref.scrollToEnd({animated:true});
  }

  render() {
    if (this.props.chats.length === 0) {
      return (
        <View style={styles.messageListContainer}>
          <Text style={styles.noChats}>Ask us anything!</Text>
        </View>
      );
    } else {
      return (
        <View style={styles.messageListContainer}>
          <FlatList
            ref={ (ref) => {
              this.ref = ref;
            } }
            onContentSizeChange={ () => {
              this.ref.scrollToEnd({animated: true});
            } }
            onLayout={ () => {
              this.ref.scrollToEnd({animated: true});
            } }
            keyExtractor={(item, index) => String(index)}
            data={this.props.chats}
            // eslint-disable-next-line max-len
            renderItem={(chat) => <MessageListItem // TODO update props for message item
              admin={chat.item.data['from_admin']}
              content={chat.item.data['content']}
              timestamp={chat.item.data['timestamp']}/>}
          />
        </View>
      );
    }
  }
}

MessageList.propTypes = {
  chats: PropTypes.arrayOf(PropTypes.object),
};

export default MessageList;
