import firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/storage';

const config = {
  apiKey: 'AIzaSyBWzGae8NuNQXxM14huxw6M7o4X7RQWG2c',
  projectId: 'i-trec',
  storageBucket: 'i-trec.appspot.com',
};

const app = firebase.initializeApp(config);
const storage = app.storage();
export const mapStorageRef = storage.ref('/venueMap/');
export const sponsorStorageRef = storage.ref('/sponsor/');
export const firestore = app.firestore();
