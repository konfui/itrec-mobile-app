/* eslint-disable react-native/no-inline-styles */
'use strict';
import PropTypes from 'prop-types';
import React from 'react';
import {TextInput, TouchableOpacity,
  KeyboardAvoidingView} from 'react-native';
import {Icon} from 'native-base';
import styles from './styles';

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {currentChat: '', height: 0};
    this.changeAction = this.changeAction.bind(this);
    this.submitChat = this.submitChat.bind(this);
  }

  changeAction(text) {
    this.setState({currentChat: text});
  }

  submitChat() {
    if (this.state.currentChat.trim() === '') return;
    this.props.submitAction(this.state.currentChat.trim());
    this.setState({currentChat: ''});
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.inputContainer}>
        <TextInput style={[styles.textInput, {
          height: Math.min(120, Math.max(35, this.state.height)),
        }]}
        multiline={true}
        value={this.state.currentChat}
        onChangeText={this.changeAction}
        onContentSizeChange={(event) =>
          this.setState({height: event.nativeEvent.contentSize.height})}
        placeholder='Type a message' />
        <TouchableOpacity
          style={styles.buttonContainer}
          onPress={this.submitChat}>
          <Icon type='SimpleLineIcons' name='cursor'
            style={styles.submitButton}
          />
        </TouchableOpacity>
      </KeyboardAvoidingView>
    );
  }
}

MessageInput.propTypes = {
  submitAction: PropTypes.func.isRequired,
};

export default MessageInput;
