'use strict';
import React from 'react';
import {View} from 'react-native';
import {Container, Content, Icon} from 'native-base';
import PropTypes from 'prop-types';
import SplashScreen from 'react-native-splash-screen';
import assets from '../../assets';
import {HeaderImage, RoundedRectangleButton,
  TextHeader} from '../../components';
import {NewsListContainer} from '../news';
import styles from './styles';

class MainMenuScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    SplashScreen.hide();
  }

  render() {
    const {navigation} = this.props;
    // %TODO% text props
    return (
      <Container>
        <Content>
          <HeaderImage source={assets.header_logo} />
          <View style={styles.shortcutContainer}>
            <RoundedRectangleButton route='EventRundown'
              style={styles.menuButton}
              text='Event<br/>Rundown'
              navigation={navigation}>
              <Icon style={styles.icon} name="list" />
            </RoundedRectangleButton>
            <RoundedRectangleButton route='PlenaryListScreen'
              text='Keynote /<br/>Invited'
              navigation={navigation}>
              <Icon style={styles.icon} name="mic" />
            </RoundedRectangleButton>
            <RoundedRectangleButton route='SymposiaList'
              style={styles.menuButton}
              text='Parallel<br/>Sessions'
              navigation={navigation}>
              <Icon style={styles.icon} name='paper' />
            </RoundedRectangleButton>
          </View>
          <TextHeader>News</TextHeader>
          <NewsListContainer navigation={navigation} />
        </Content>
      </Container>
    );
  }
}

MainMenuScreen.propTypes = {
  navigation: PropTypes.object,
};

export default MainMenuScreen;
