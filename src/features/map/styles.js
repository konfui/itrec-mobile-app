'use strict';
import {Dimensions, StyleSheet} from 'react-native';
import qir from '../../../native-base-theme/variables/qir';

const win = Dimensions.get('window');
const ratio = win.width/541;

export default StyleSheet.create({
  header: {
    backgroundColor: qir.brandPrimary,
  },
  listItemContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 8,
  },
  listItemIcon: {
    fontSize: 36,
  },
  listItemLeftColumn: {
    flex: 3,
    flexDirection: 'column',
  },
  listItemRightColumn: {
    alignItems: 'flex-end',
    flex: 1,
    marginEnd: 16,
  },
  listItemTimePlaceText: {
    fontSize: 14,
    marginTop: 2,
  },
  listItemTitleText: {
    fontWeight: 'bold',
  },
  container: {
    backgroundColor: qir.brandPrimary,
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 8,
  },
  // eslint-disable-next-line react-native/no-color-literals
  title: {
    color: 'white', // %TODO%
    fontWeight: 'bold',
    fontSize: 16,
    marginHorizontal: 2,
    textTransform: 'uppercase',
  },
  image: {
    height: 362*ratio,
    width: win.width,
  },
});

