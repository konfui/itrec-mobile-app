'use strict';
import {Container, Content, H1, Text} from 'native-base';
import {View, Image} from 'react-native';
import React from 'react';
import styles from './styles';
import {withFirebaseImage} from '../../components';

const VenueMapScreen = () => {
  // %TODO% Images of map
  const Map01 = withFirebaseImage(Image, '/venueMap/map_01.png');
  const Map02 = withFirebaseImage(Image, '/venueMap/map_02.png');
  const Map03 = withFirebaseImage(Image, '/venueMap/map_03.png');
  // %TODO% Location name
  return (
    <Container>
      <Content>
        <Text style={styles.container}>
          <H1 style={styles.title}>The Anvaya Beach Resort Bali</H1>
        </Text>
        <View>
          <Map01 resizeMode='contain' style={styles.image} />
          <Map02 resizeMode='contain' style={styles.image} />
          <Map03 resizeMode='contain' style={styles.image} />
        </View>
      </Content>
    </Container>
  );
};

export default VenueMapScreen;
