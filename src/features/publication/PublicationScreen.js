'use strict';
import {Container, Content, Text} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import styles from './styles';
import {HeaderBurger} from '../../components';

// %TODO% Content
const PublicationScreen = ({navigation}) =>
  <Container>
    <HeaderBurger title='Publication' navigation={navigation}/>
    <Content>
      <Text style={styles.itemStyle}>
        After peer-reviewed, accepted and presented papers
        will be published in special / regular issues of the
        following journals or proceeding:{'\n'}{'\n'}
        {'\u2022'} International Journal of Technology (ranks
          in Q2 and indexed in SCOPUS).{'\n'}
        {'\u2022'} Engineering Journal (ranks in Q2 in the General
          Engineering subject category, and is indexed in SCOPUS){'\n'}
        {'\u2022'} Evergreen – Joint Journal of Novel Carbon Resource
          Sciences and Green Asia Strategy, Kyushu University (ranks in
          Q3 and indexed in SCOPUS){'\n'}
        {'\u2022'} Indonesian Journal of Chemistry (rank in Q3 and
          indexed in SCOPUS){'\n'}
        {'\u2022'} AIP Conference Proceedings (The Conference Proceedings
          Citation Index Scopus).{'\n'}
      </Text>
    </Content>
  </Container>;

PublicationScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default PublicationScreen;
