'use strict';
import React from 'react';
import {TouchableHighlight} from 'react-native';
import {Text} from 'native-base';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import {shallow} from 'enzyme';
import SubmissionListItem from '../SubmissionListItem';

describe('A submission list item', () => {
  let mockNavigation = {};

  beforeEach(() => {
    mockNavigation = {push: jest.fn()};
  });

  it('matches with the snapshot', () => {
    const component = TestRenderer.create(
        <SubmissionListItem title="The Circle of Life"
          authors="Simba and Friends" organization="The Lion King"
          onPress={mockNavigation.push} />
    );

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('contains a pressable text', () => {
    const wrapper = shallow(
        <SubmissionListItem title="The Circle of Life"
          authors="Simba and Friends" organization="The Lion King"
          onPress={mockNavigation.push} />
    );

    const pressableComponent = wrapper.find(TouchableHighlight);

    expect(pressableComponent.length).toEqual(1);
    expect(pressableComponent.first().children(Text).length).toEqual(1);
  });

  it('pressable', () => {
    const wrapper = shallow(
        <SubmissionListItem title="The Circle of Life"
          authors="Simba and Friends" organization="The Lion King"
          onPress={mockNavigation.push} />
    );

    wrapper.find(TouchableHighlight).simulate('press');

    expect(mockNavigation.push).toBeCalled();
  });
});
