'use strict';
import NetInfo from '@react-native-community/netinfo';
import {Body, Button, Card, CardItem, Container, Content, Header, Icon, Input,
  Item, Spinner, Text} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {TextHeader2} from '../../components';
import {ConnectionStatus, getData, LoadStatus} from '../../utils';
import styles from './styles';
import SubmissionList from './SubmissionList';
const DEFAULT_TRACK = 'SGR';

class SubmissionListScreen extends React.Component {
  constructor(props) {
    super(props);
    const {navigation} = this.props;

    this.state = {
      data: [],
      track: navigation.getParam('track', DEFAULT_TRACK),
      query: '',
      connectionStatus: ConnectionStatus.OFFLINE,
      loadStatus: LoadStatus.EMPTY,
    };

    this._resetState = this._resetState.bind(this);
    this._connectionChangeListener = this._connectionChangeListener.bind(this);
    this._loadData = this._loadData.bind(this);
    this._unloadData = this._unloadData.bind(this);
  }
  componentDidMount() {
    console.log('SubmissionListScreen#componentDidMount():',
        'Add connection change listener');
    NetInfo.addEventListener('connectionChange',
        this._connectionChangeListener);
  }
  componentWillUnmount() {
    console.log('SubmissionListScreen#componentWillUnmount():',
        'Remove connection change listener');
    NetInfo.removeEventListener('connectionChange',
        this._connectionChangeListener);

    console.log('SubmissionListScreen#componentWillUnmount():',
        'Reset state');
    this._resetState();
  }

  render() {
    const {navigation} = this.props;
    const {data, loadStatus, query} = this.state;

    return (
      <Container>
        <Header style={styles.header}>
          <TextHeader2 first="Parallel" second="Sessions" />
        </Header>
        <Content>
          <Item underline>
            <Input style={styles.input}
              placeholder="Search by author, paper, or keywords"
              placeholderTextColor="gray"
              onChangeText={(text) => this.setState({query: text})}
              onSubmitEditing={() => {
                navigation.push('SearchResultScreen',
                    {query: query});
              }}
            />
            <Button style={styles.search} small transparent onPress={() => {
              navigation.push('SearchResultScreen',
                  {query: query});
            }}>
              <Icon name="magnifier" type="SimpleLineIcons"/>
            </Button>
          </Item>
          {loadStatus === LoadStatus.LOADING && <Spinner />}
          {loadStatus === LoadStatus.ERROR &&
            this._renderError(this.props.errorMessage)}
          {loadStatus === LoadStatus.EMPTY &&
            this._renderError('You are currently offline')}
          {loadStatus === LoadStatus.LOADED &&
            <SubmissionList data={data} navigation={navigation} />
          }
        </Content>
      </Container>
    );
  }

  _onPress() {
    const {navigation} = this.props;
    const {query} = this.state;
    console.log('SubmissionListScreen#_onPress()',
        'Given query', query);

    if (query.length >= 2) {
      navigation.push('SearchResultScreen', {query: query});
    } else {
      Toast.show({
        // eslint-disable-next-line
        text: 'Length of search query must longer than or equal to 2 characters',
        buttonText: 'Dismiss',
        position: 'top',
      });
    }
  }

  _renderError(message) {
    return (
      <Card>
        <CardItem>
          <Body>
            {console.log('SubmissionListScreen#_renderError():',
                'Rendering error message', message)}
            <Text>{message}</Text>
          </Body>
        </CardItem>
      </Card>
    );
  }

  _resetState() {
    this.setState({
      data: [],
      query: '',
      connectionStatus: ConnectionStatus.OFFLINE,
      loadStatus: LoadStatus.EMPTY,
    });
  }

  _connectionChangeListener(connectionInfo) {
    switch (connectionInfo.type) {
      case 'none':
      case 'unknown':
        if (this.state.connectionStatus === ConnectionStatus.ONLINE) {
          console.log('SubmissionListScreen#_connectionChangeListener():',
              'Connection switched from online to offline');
          this.setState({connectionStatus: ConnectionStatus.OFFLINE});
          this._unloadData();
        }
        break;
      case 'wifi':
      case 'cellular':
        if (this.state.connectionStatus === ConnectionStatus.OFFLINE) {
          console.log('SubmissionListScreen#_connectionChangeListener():',
              'Connection switched from offline to online');
          this.setState({connectionStatus: ConnectionStatus.ONLINE});
          this._loadData();
        }
        break;
      default:
        console.log('SubmissionListScreen#_connectionChangeListener():',
            'Unrecognised connection type', connectionInfo.type);
        break;
    }
  }

  _loadData() {
    const {endpoint} = this.props;
    const {track} = this.state;

    switch (this.state.loadStatus) {
      case LoadStatus.LOADED: // TODO: Reload data from cache instead of API
      case LoadStatus.EMPTY:
        console.log('SubmissionListScreen#_loadData():',
            'Loading data from:', endpoint);
        this.setState({loadStatus: LoadStatus.LOADING});

        getData(endpoint, {track: track}).then((json) => {
          console.log('SubmissionListScreen#_loadData():',
              'Received data in JSON format', json);
          this.setState({
            data: json.data,
            loadStatus: LoadStatus.LOADED,
          });
        }).catch((error) => {
          console.error('SubmissionListScreen#_loadData():',
              'There was a problem communicating with the API at:',
              endpoint);
          console.error(error);
          this.setState({loadStatus: LoadStatus.ERROR});
        });
        break;
      case LoadStatus.LOADING:
      case LoadStatus.ERROR:
        break; // Do nothing
      default:
        break; // Do nothing
    }
  }

  _unloadData() {
    switch (this.state.loadStatus) {
      // TODO: Store data into cache instead of clearing it
      case LoadStatus.LOADED:
        console.log('SubmissionListScreen#_unloadData():',
            'Unload data');
        this.setState({
          data: [],
          loadStatus: LoadStatus.EMPTY,
        });
        break;
      case LoadStatus.EMPTY:
      case LoadStatus.LOADING:
      case LoadStatus.ERROR:
        break; // Do nothing
      default:
        break; // Do nothing
    }
  }
}

SubmissionListScreen.propTypes = {
  endpoint: PropTypes.string,
  errorMessage: PropTypes.string,
  navigation: PropTypes.object.isRequired,
};

SubmissionListScreen.defaultProps = {
  endpoint: 'https://asia-northeast1-i-trec.cloudfunctions.net/publications',
  errorMessage: 'There is a problem with the app or the Internet connection',
};

export default SubmissionListScreen;
