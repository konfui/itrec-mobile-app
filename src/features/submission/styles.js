'use strict';
import {StyleSheet} from 'react-native';
import qir from '../../../native-base-theme/variables/qir';

export default StyleSheet.create({
  contentColumn: {
    flex: 5,
    flexDirection: 'column',
  },
  listItem: {
    flexDirection: 'row',
    marginBottom: 8,
  },
  detailAbstractHeadingText: {
    backgroundColor: qir.brandPrimary,
    color: qir.brandWhite,
    textTransform: 'uppercase',
    marginVertical: 8,
    padding: 8,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  header: {
    backgroundColor: qir.brandPrimary,
  },
  horizontal: {
    alignItems: 'flex-start',
    flexDirection: 'row',
  },
  organization: {
    fontSize: 12,
  },
  separator: {
    borderWidth: 1,
    borderColor: qir.brandSoft,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 8,
  },
  titleList: {
    fontSize: 16,
    fontWeight: 'bold',
    marginVertical: 4,
  },
  authorsList: {
    fontSize: 14,
    marginVertical: 4,
  },
  viewDetail: {
    color: qir.brandAccent,
    textTransform: 'uppercase',
    fontSize: 12,
    marginVertical: 4,
  },
  iconStyle: {
    fontSize: 14,
  },
  itemStyle: {
    color: qir.brandGrey,
    fontSize: 14,
  },
  indexColumn: {
    alignSelf: 'center',
    flex: 0,
    marginHorizontal: 12,
  },
  input: {
    fontSize: 14,
  },
  search: {
    alignSelf: 'center',
  },
});
