'use strict';
import React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import AuthorListContainer from '../AuthorListContainer';
import sampleData from '../sampleData';

describe('An author list container component', () => {
  let mockNavigation = {};

  beforeEach(() => {
    mockNavigation = {push: jest.fn()};
  });

  it('matches with the snapshot', () => {
    const component = TestRenderer.create(
        <AuthorListContainer data={sampleData} navigation={mockNavigation} />
    );

    expect(component.toJSON()).toMatchSnapshot();
  });
});
