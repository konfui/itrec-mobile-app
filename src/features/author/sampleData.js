'use strict';
export default [
  {
    personId: '1',
    fullName: 'Giorno Giovanna',
    email: 'giorno@passione.it',
    organization: 'Gangstar',
  },
  {
    personId: '2',
    fullName: 'Guido Mista',
    email: 'mista@passione.it',
    organization: 'Gangstar',
  },
  {
    personId: '3',
    fullName: 'Mister Polnareff',
    email: 'polnareff@theog.com',
    organization: 'Jojofans',
  },
  {
    personId: '4',
    fullName: 'Trish Una',
    email: 'trish@passione.it',
    organization: 'Gangstar',
  },
  {
    personId: '5',
    fullName: 'John Smith',
    email: 'john@smith.it',
    organization: 'John Doe',
  },
];
