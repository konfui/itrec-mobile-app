'use strict';
import NetInfo from '@react-native-community/netinfo';
import {Body, Button, Card, CardItem, Icon, Input, Item, Spinner,
  Text} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {FlatList, View} from 'react-native';
import {ConnectionStatus, getData, LoadStatus} from '../../utils';
import {ListItem} from './../../components';
import styles from './styles';

class AuthorListContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data ? props.data : [],
      query: '',
      connectionStatus: ConnectionStatus.OFFLINE,
      loadStatus: props.data ? LoadStatus.LOADED : LoadStatus.EMPTY,
    };
    this._resetState = this._resetState.bind(this);
    this._connectionChangeListener = this._connectionChangeListener.bind(this);
    this._loadData = this._loadData.bind(this);
    this._unloadData = this._unloadData.bind(this);
  }
  componentDidMount() {
    console.log('AuthorListContainer#componentDidMount():',
        'Add connection change listener');
    NetInfo.addEventListener('connectionChange',
        this._connectionChangeListener);
  }

  componentWillUnmount() {
    console.log('AuthorListContainer#componentWillUnmount():',
        'Remove connection change listener');
    NetInfo.removeEventListener('connectionChange',
        this._connectionChangeListener);

    console.log('AuthorListContainer#componentWillUnmount():',
        'Reset state');
    this._resetState();
  }

  render() {
    if (this.state.loadStatus === LoadStatus.LOADING) {
      return this._renderLoading();
    } else if (this.state.loadStatus === LoadStatus.LOADED) {
      return this._renderLoaded();
    } else if (this.state.loadStatus === LoadStatus.ERROR) {
      return this._renderError(this.props.errorMessage);
    } else { // (Empty data/offline)
      return this._renderError('You are currently offline');
    }
  }

  _renderLoading() {
    return <Spinner />;
  }

  _renderLoaded() {
    const {navigation} = this.props;
    console.log('AuthorListContainer#_renderLoaded():',
        'Rendering...');
    console.log('AuthorListContainer#_renderLoaded():',
        '# of authors in the data state:', this.state.data.length);
    return (
      <View>
        <Item underline>
          <Input style={styles.input} placeholder="Search author(s) by name"
            placeholderTextColor="gray"
            onChangeText={(text) => this.setState({query: text})} />
          <Button style={styles.search} transparent small>
            <Icon name="magnifier" type="SimpleLineIcons"/>
          </Button>
        </Item>
        <FlatList
          data={filterByName(this.state.data, this.state.query)}
          keyExtractor={(item, id) =>
            String(`${item.fullName}${id}`)}
          renderItem={({item}) =>
            <ListItem onPress={() => navigation.push('AuthorDetailScreen',
                {author: item})}>
              <Text>{item.fullName}</Text>
            </ListItem>}/>
      </View>
    );
  }

  _renderError(message) {
    return (
      <Card>
        <CardItem>
          <Body>
            {console.log('AuthorListContainer#_renderError():',
                'Rendering error message', message)}
            <Text>{message}</Text>
          </Body>
        </CardItem>
      </Card>
    );
  }

  _resetState() {
    this.setState({
      data: [],
      query: '',
      connectionStatus: ConnectionStatus.OFFLINE,
      loadStatus: LoadStatus.EMPTY,
    });
  }

  _connectionChangeListener(connectionInfo) {
    switch (connectionInfo.type) {
      case 'none':
      case 'unknown':
        if (this.state.connectionStatus === ConnectionStatus.ONLINE) {
          console.log('AuthorListContainer#_connectionChangeListener():',
              'Connection switched from online to offline');
          this.setState({connectionStatus: ConnectionStatus.OFFLINE});
          this._unloadData();
        }
        break;
      case 'wifi':
      case 'cellular':
        if (this.state.connectionStatus === ConnectionStatus.OFFLINE) {
          console.log('AuthorListContainer#_connectionChangeListener():',
              'Connection switched from offline to online');
          this.setState({connectionStatus: ConnectionStatus.ONLINE});
          this._loadData();
        }
        break;
      default:
        console.log('AuthorListContainer#_connectionChangeListener():',
            'Unrecognised connection type', connectionInfo.type);
        break;
    }
  }
  _loadData() {
    const {endpoint} = this.props;
    switch (this.state.loadStatus) {
      case LoadStatus.LOADED: // TODO: Reload data from cache instead of API
      case LoadStatus.EMPTY:
        console.log('AuthorListContainer#_loadData():',
            'Loading authors data from:', endpoint);
        this.setState({loadStatus: LoadStatus.LOADING});

        getData(endpoint).then((json) => {
          console.log('AuthorListContainer#_loadData():',
              'Received authors data in JSON format', json);
          this.setState({
            data: json.data,
            loadStatus: LoadStatus.LOADED,
          });
        }).catch((error) => {
          console.error('AuthorListContainer#_loadData():',
              'There was a problem communicating with authors API at:',
              endpoint);
          console.error(error);
          this.setState({loadStatus: LoadStatus.ERROR});
        });
        break;
      case LoadStatus.LOADING:
      case LoadStatus.ERROR:
        break; // Do nothing
      default:
        break; // Do nothing
    }
  }

  _unloadData() {
    switch (this.state.loadStatus) {
      // TODO: Store data into cache instead of clearing it
      case LoadStatus.LOADED:
        console.log('AuthorListContainer#_unloadData():',
            'Unload data');
        this.setState({
          data: [],
          loadStatus: LoadStatus.EMPTY,
        });
        break;
      case LoadStatus.EMPTY:
      case LoadStatus.LOADING:
      case LoadStatus.ERROR:
        break; // Do nothing
      default:
        break; // Do nothing
    }
  }
}

const filterByName = (data, name) => {
  if (name === '') return data;
  const filteredData = data.filter((entry) =>
    entry.fullName.toLowerCase().includes(name.toLowerCase()));
  return filteredData;
};


AuthorListContainer.propTypes = {
  endpoint: PropTypes.string,
  errorMessage: PropTypes.string,
  data: PropTypes.arrayOf(PropTypes.object),
  navigation: PropTypes.object.isRequired,
};

AuthorListContainer.defaultProps = {
  errorMessage: 'There is a problem with the app or the Internet connection',
  endpoint: 'https://asia-northeast1-i-trec.cloudfunctions.net/authors',
};

export default AuthorListContainer;
