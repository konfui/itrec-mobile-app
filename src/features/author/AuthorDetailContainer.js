'use strict';
import NetInfo from '@react-native-community/netinfo';
import {Spinner, Text} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {getData, Status} from '../../utils';
import {FlatList, View} from 'react-native';
import AuthorDetailItem from './AuthorDetailItem';
import styles from './styles';

class AuthorDetailContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status: props.data ? Status.SUCCESS : Status.OFFLINE,
      data: props.data ? props.data : [],
    };
    this.renderAuthorDetailItem = this.renderAuthorDetailItem.bind(this);
  }

  componentDidMount() {
    if (this.state.status !== Status.SUCCESS) {
      NetInfo.isConnected.fetch().then((isConnected) => {
        if (isConnected) {
          this.setState({status: Status.LOADING});
          getData(this.props.endpoint).then((json) => {
            console.log('AuthorDetailContainer: Retrieved publications:');
            console.log(this.state.data);
            this.setState({
              status: Status.SUCCESS,
              data: json.data,
            });
          }).catch((error) => {
            console.warn(error);
            this.setState({status: Status.ERROR});
          });
        }
      });
    }
  }

  componentWillUnmount() {
    this.setState({data: []});
  }

  render() {
    if (this.state.status === Status.LOADING) {
      console.log('AuthorDetailContainer: Loading...');
      return <Spinner />;
    } else if (this.state.status === Status.SUCCESS) {
      return this.renderAuthorDetail();
    } else if (this.state.status === Status.OFFLINE) {
      return <Text>You are offline!</Text>;
    } else {
      return <Text>An error has occurred!</Text>;
    }
  }

  renderAuthorDetail() {
    console.log('AuthorDetailContainer#_RenderAuthorDetail: Rendering...');
    const {authorName} = this.props;
    const filteredData = filterByName(this.state.data, authorName);
    return <FlatList data={filteredData}
      keyExtractor={(item, _) => item.id}
      ItemSeparatorComponent={() => <View style={styles.separator}></View>}
      renderItem={({item}) => this.renderAuthorDetailItem(item)}
      ListEmptyComponent={
        <View style={styles.container}>
          <Text>This Author has no session</Text>
        </View>
      } />;
  }

  renderAuthorDetailItem(item) {
    const {navigation} = this.props;
    const fromSearch = navigation.getParam('fromSearch', false);
    const submission = createSubmission(item);
    const pressHandler = (fromSearch) ?
      () => navigation.push('SubmissionDetailScreen',
          {submission: submission}) :
      () => navigation.push('AuthorSubmissionDetailScreen',
          {submission: submission});

    return (
      <AuthorDetailItem title={item.title} date={item.date} time={item.time}
        location={item.location}
        onPress={pressHandler} />
    );
  }
};

function createSubmission(item) {
  const submissionObject = {
    title: item.title,
    date: item.date,
    time: item.time,
    location: item.location,
    chair: item.chair,
    authors: item.authors,
    abstract: item.abstract,
  };
  console.log(`AuthorDetailContainer#createSubmission ${submissionObject}`);
  return submissionObject;
}

function filterByName(data, name) {
  console.log(`AuthorDetailContainer#_RenderAuthorDetail: Filtered by ${name}`);
  if (name === '') return data;
  const filteredData = data.filter((entry) =>
    entry.authors.toLowerCase().includes(name.toLowerCase()));
  console.log(`# of publications by ${name}`,
      `is ${filteredData.length} :${filteredData} `);
  return filteredData;
};

AuthorDetailContainer.propTypes = {
  navigation: PropTypes.object.isRequired,
  data: PropTypes.arrayOf(PropTypes.object),
  poster: PropTypes.bool,
  endpoint: PropTypes.string,
  authorName: PropTypes.string,
};

AuthorDetailContainer.defaultProps = {
  poster: false,
  endpoint: 'https://asia-northeast1-i-trec.cloudfunctions.net/publications',
};

export default AuthorDetailContainer;
