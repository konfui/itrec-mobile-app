'use strict';
import {Container, Content, Icon, Text, Thumbnail} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {View} from 'react-native';
import {TextHeader} from '../../components';
import styles from './styles';
import {withFirebaseImage} from '../../components';
import assets from '../../assets';

const EventDetailScreen = ({navigation}) => {
  const {name, affiliation, country, profile, speechAbstract, speechTitle,
    storagePath, time, room, symposium} = navigation.getParam('keynote', {});
  console.log('EventDetailScreen: Param ', storagePath);
  const SpeakerThumbnail = withFirebaseImage(Thumbnail, storagePath,
      assets.profile_placeholder);

  return (
    <Container>
      <Content padder>
        <View style={styles.listItem}>
          <SpeakerThumbnail large />
          <View style={styles.speakerContainer}>
            <Text style={styles.name}>{name}</Text>
            <Text style={styles.itemStyle}>{affiliation}</Text>
            <Text style={styles.itemStyle}>{country}</Text>
          </View>
        </View>
        <TextHeader>Sessions</TextHeader>
        <View style={styles.container}>
          <Text style={styles.sessionDetail}>{symposium}</Text>
          <Text style={styles.name}>{speechTitle}</Text>
          <View style={styles.horizontal}>
            <Icon style={styles.iconStyle}
              type="SimpleLineIcons" name="clock" />
            <Text style={styles.sessionDetail}>   {time}</Text>
          </View>
          <View style={styles.horizontal}>
            <Icon style={styles.iconStyle}
              type="SimpleLineIcons" name="location-pin" />
            <Text style={styles.sessionDetail}>   {room}</Text>
          </View>
        </View>
        <TextHeader>Abstract</TextHeader>
        <Text style={styles.detailStyle}>{speechAbstract}</Text>
        <TextHeader>Biography</TextHeader>
        <Text style={styles.detailStyle}>{profile}</Text>
      </Content>
    </Container>
  );
};

EventDetailScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default EventDetailScreen;

