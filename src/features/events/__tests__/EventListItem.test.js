'use strict';
import React from 'react';
import 'react-native';
import {shallow} from 'enzyme';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import EventListItem from '../EventListItem';
import {Icon} from 'native-base';

describe('An event list item component', () => {
  let mockNavigation = null;
  beforeEach(() => {
    mockNavigation = {
      push: jest.fn(),
    };
  });

  it('matches its snapshot correctly', () => {
    const componentWithoutSpeaker = TestRenderer.create(
        <EventListItem
          title='Test Without Speaker'
          date='22-07-2019'
          time='10:00 - 13:00'
          place='Test Hall' />
    );
    const componentWithSpeaker = TestRenderer.create(
        <EventListItem
          title='Test With Speaker'
          date='22-07-2019'
          time='09:00 - 09:55'
          place='Test Ballroom'
          speaker='Giorno Giovanna' />
    );
    const componentWithoutPlace = TestRenderer.create(
        <EventListItem
          title='Test Without Place'
          date='23-07-2019'
          time='15:00 - 16:00'
          speaker='Dio' />
    );

    expect(componentWithoutSpeaker.toJSON()).toMatchSnapshot();
    expect(componentWithSpeaker.toJSON()).toMatchSnapshot();
    expect(componentWithoutPlace.toJSON()).toMatchSnapshot();
  });

  it('can be clicked', () => {
    const wrapper = shallow(
        <EventListItem
          title='Shallow Test'
          date='23-07-2019'
          time='10:00 - 13:00'
          place='Test Place'
          speaker='Prof. Giorno Giovanna'
          navigation={mockNavigation} />
    );

    wrapper.simulate('press');

    expect(mockNavigation.push).toBeCalled();
    expect(mockNavigation.push.mock.calls[0][0]).toBe('EventDetail');

    /* expect(mockNavigation.push.mock.calls[0][1]).toEqual({
      speaker: 'Prof. Giorno Giovanna',
      date: '23-07-2019',
      time: '10:00 - 13:00',
      place: 'Test Place',
    }); */
  });

  describe('when it is a symposia event', () => {
    beforeEach(() => {
      mockNavigation = {
        push: jest.fn(),
      };
    });

    it('matches its snapshot', () => {
      const component = TestRenderer.create(
          <EventListItem
            title='Symposia Test'
            date='22-07-2019'
            time='10:00 - 13:00'
            place='Test Hall'
            navigation={mockNavigation}
            isParallel
          />
      );

      expect(component.toJSON()).toMatchSnapshot();
    });

    it('can be clicked', () => {
      const wrapper = shallow(
          <EventListItem
            title='Symposia Test'
            date='22-07-2019'
            time='10:00 - 13:00'
            place='Test Hall'
            navigation={mockNavigation}
            isParallel
          />
      );

      wrapper.simulate('press');

      expect(mockNavigation.push).toBeCalled();
      expect(mockNavigation.push.mock.calls[0][0]).toBe('SymposiaList');
    });

    it('contains arrow when title starts with parallel', () => {
      const wrapper = shallow(
          <EventListItem
            title='Parallel Test'
            date='22-07-2019'
            time='10:00 - 13:00'
            place='Test Hall'
            navigation={mockNavigation}
            isParallel
          />
      ).dive();

      expect(
          wrapper.containsMatchingElement(<Icon/>)
      ).toBeTruthy();
    });

    it('contains arrow when speaker exists', () => {
      const wrapper = shallow(
          <EventListItem
            title='Symposia Test'
            date='22-07-2019'
            time='10:00 - 13:00'
            place='Test Hall'
            speaker='Prof. Giorno Giovanna'
            navigation={mockNavigation}
          />
      ).dive();

      expect(
          wrapper.containsMatchingElement(<Icon/>)
      ).toBeTruthy();
    });

    it('no arrow when no speaker exists and not parallel', () => {
      const wrapper = shallow(
          <EventListItem
            title='Symposia Test'
            date='22-07-2019'
            time='10:00 - 13:00'
            place='Test Hall'
            navigation={mockNavigation}
          />
      );

      expect(
          wrapper.find('icon')
      ).toEqual({});
    });
  });
});
