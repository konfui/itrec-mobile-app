'use strict';
import {Text} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {View} from 'react-native';
import styles from './styles';

const DetailListItem = ({place, title}) =>
  <View style={styles.listItemChild}>
    <View style={styles.childPadding} />
    <View style={styles.childContent}>
      <Text style={styles.childPlace}>{place} : </Text>
      <Text style={styles.childTitle}>{title}</Text>
    </View>
  </View>;


DetailListItem.propTypes = {
  place: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

export default DetailListItem;
