// SPDX-License-Identifier: GPL-3.0-or-later
'use strict';
import {Body, Button, Container, Content, Header, Segment, Spinner, Tab, Tabs,
  Text} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {Platform} from 'react-native';
import {TextHeader2} from '../../components';
import {getData} from '../../utils';
import createEventTabHeading from './createEventTabHeading';
import EventList from './EventList';
import styles from './styles';
const AUGUST = 7;
const DATES = [
  {day: new Date(Date.UTC(2019, AUGUST, 14)), title: 'Conference'},
  {day: new Date(Date.UTC(2019, AUGUST, 15)), title: 'Conference'},
  {day: new Date(Date.UTC(2019, AUGUST, 16)), title: 'Free Time'},
];

class EventListScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
      loading: false,
      loaded: false,
      data: [],
      speakers: [],
      activeSegment: 0, // Active segment in iOS build
    };
  }

  async componentDidMount() {
    this.setState({loading: true});
    try {
      const {endpoint, speakersEndpoint} = this.props;
      const {data} = await getData(endpoint);
      const speakersJSON = await getData(speakersEndpoint);
      const speakers = speakersJSON.data;
      console.log('EventListScreen#componentDidMount()',
          'Received data from:', endpoint, data);
      console.log('EventListScreen#componentDidMount()',
          'Received speakers data from:', endpoint, speakers);
      this.setState({
        loading: false,
        loaded: true,
        data: data,
        speakers: speakers,
      });
    } catch (error) {
      console.error('EventListScreen#componentDidMount()',
          'Error:', error);
      this.setState({
        hasError: true,
        loading: false,
        loaded: false,
      });
    }
  }

  render() {
    if (this.state.hasError) {
      return this.renderError();
    }
    if (this.state.loading) {
      return this.renderLoading();
    } else {
      if (Platform.OS !== 'ios') {
        return this.renderAndroid();
      } else {
        return this.renderIOS();
      }
    }
  }

  renderError() {
    return (
      <Container>
        <Content>
          <Text>
            Oops! There is a problem with the app or the network connection.
          </Text>
        </Content>
      </Container>
    );
  }

  renderLoading() {
    return (
      <Container>
        <Content>
          <Spinner />
        </Content>
      </Container>
    );
  }

  /* eslint-disable */
  renderIOS() {
    const numOfDays = DATES.length;
    const {day} = DATES[this.state.activeSegment];
    // %TODO% backgroundColor
    return (
      <Container>
        <Header hasSegment style={{backgroundColor: '#f8f8f8'}}>
          <Body>
            <Segment>
              {
                DATES.map((date, index) => {
                  if (index === 0) {
                    return (
                      <Button key={index}
                        active={this.state.activeSegment === index}
                        first
                        onPress={() => this.setState({activeSegment: index})}>
                        <Text>{`Day ${index + 1}`}</Text>
                      </Button>
                    );
                  } else if (index === numOfDays-1) {
                    return (
                      <Button key={index}
                        active={this.state.activeSegment === index}
                        last
                        onPress={() => this.setState({activeSegment: index})}>
                        <Text>{`Day ${index + 1}`}</Text>
                      </Button>
                    );
                  } else {
                    return (
                      <Button key={index}
                        active={this.state.activeSegment === index}
                        onPress={() => this.setState({activeSegment: index})}>
                        <Text>{`Day ${index + 1}`}</Text>
                      </Button>
                    );
                  }
                })
              }
            </Segment>
          </Body>
        </Header>
        <Content>
          {this.renderEventList(day)}
        </Content>
      </Container>
    );
  }
  /* eslint-enable */

  renderAndroid() {
    return (
      <Container>
        <Header hasTabs style={styles.header}>
          <TextHeader2 first="Event" second="Rundown" />
        </Header>
        <Content>
          <Tabs tabBarPosition="overlayTop">
            {
              DATES.map((date) => {
                const {day, title} = date;
                return (
                  <Tab key={day}
                    heading={createEventTabHeading(day, title)}>
                    {this.renderEventList(day)}
                  </Tab>
                );
              })
            }
          </Tabs>
        </Content>
      </Container>);
  }

  renderEventList(day) {
    const {navigation} = this.props;
    return <EventList navigation={navigation}
      keynoteData={this.state.speakers}
      eventData={this.state.data.filter((event) =>
        event.date.substring(0, 2) === String(day.getDate())
      )}
    />;
  }
}

EventListScreen.propTypes = {
  endpoint: PropTypes.string,
  navigation: PropTypes.object,
  speakersEndpoint: PropTypes.string,
  present: PropTypes.bool,
  data: PropTypes.arrayOf(PropTypes.object),
};

EventListScreen.defaultProps = {
  endpoint: 'https://asia-northeast1-i-trec.cloudfunctions.net/getEvents',
  speakersEndpoint: 'https://asia-northeast1-i-trec.cloudfunctions.net/speakers',
};

export default EventListScreen;
