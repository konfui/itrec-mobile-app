'use strict';
import {StyleSheet} from 'react-native';
import qir from '../../../native-base-theme/variables/qir';

export default StyleSheet.create({
  // iOS colour taken from default color of NativeBase's Segment
  header: {
    backgroundColor: qir.brandPrimary,
  },
  listItemContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 8,
  },
  listItemIcon: {
    fontSize: 16,
  },
  listItemLeftColumn: {
    flex: 3,
    flexDirection: 'column',
  },
  listItemRightColumn: {
    alignItems: 'flex-end',
    flex: 1,
    marginEnd: 16,
  },
  listItemTimePlaceText: {
    fontSize: 14,
    marginTop: 2,
  },
  listItemTitleText: {
    fontWeight: 'bold',
  },
  style: {
    backgroundColor: qir.brandSecondary,
  },
  lineHeight: {
    lineHeight: 10,
    fontSize: 10,
    textAlign: 'center',
  },
  listItemChild: {
    flexDirection: 'row',
    padding: 2,
  },
  childPadding: {
    flex: 1,
  },
  childContent: {
    flex: 4,
  },
  childPlace: {
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'flex-start',
  },
  childTitle: {
    fontSize: 14,
    alignSelf: 'flex-start',
  },
  container: {
    alignItems: 'flex-start',
    flexDirection: 'column',
    padding: 8,
  },
  contentColumn: {
    flex: 3,
    flexDirection: 'column',
  },
  detailStyle: {
    color: qir.brandGrey,
    fontSize: 14,
    padding: 8,
  },
  header: {
    backgroundColor: qir.brandPrimary,
  },
  horizontal: {
    alignItems: 'flex-start',
    flexDirection: 'row',
    marginVertical: 4,
  },
  iconStyle: {
    fontSize: 14,
  },
  index: {
    fontWeight: 'bold',
  },
  indexColumn: {
    alignSelf: 'center',
    flex: 1,
  },
  input: {
    fontSize: 14,
  },
  itemStyle: {
    fontStyle: 'italic',
    color: qir.brandGrey,
    fontSize: 14,
  },
  listItem: {
    flexDirection: 'row',
    marginBottom: 8,
  },
  name: {
    fontWeight: 'bold',
  },
  photoIcon: {
    fontSize: 64,
  },
  search: {
    alignSelf: 'center',
  },
  sessionDetail: {
    color: qir.brandGrey,
    fontSize: 12,
  },
  speakerContainer: {
    flexDirection: 'column',
    marginLeft: 8,
  },
  text: {
    marginVertical: 16,
  },
});
