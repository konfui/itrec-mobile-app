'use strict';
import {Container, Content, Text} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import styles from './styles';
import {HeaderBurger} from '../../components';

const ContactScreen = ({navigation}) =>
  <Container>
    <HeaderBurger title='Organizer' navigation={navigation}/>
    <Content>
      <Text style={styles.textMargin}>
        TREC FTUI Room, EC. 308-309, Engineering Center Building,{'\n'}
        Faculty of Engineering, Universitas Indonesia 16424{'\n'}
        Phone: +62-21-7863504{'\n'}
        Kampus UI Depok{'\n'}
        16424{'\n'}
        E-mail:
        <Text style={styles.textBold}>
          i-trec@ui.ac.id
        </Text>
      </Text>
    </Content>
  </Container>;

ContactScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default ContactScreen;
