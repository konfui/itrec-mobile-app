'use strict';
import React from 'react';
import {Button, Container, Content, Icon, Input, Item,
  Toast} from 'native-base';
import {TextHeader, TextHeader2} from '../../components';
import SearchResultList from './SearchResultList';
import PropTypes from 'prop-types';
import styles from './styles';
const EMPTY_TEXT = '';

class SearchResultScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nextQuery: '',
    };
    this._onPress = this._onPress.bind(this);
  }

  render() {
    const {navigation, placeholderText} = this.props;
    const query = navigation.getParam('query', EMPTY_TEXT);

    // %TODO% first & second props
    return (
      <Container>
        <TextHeader2 first="Search" second="Results" />
        <Content>
          <Item underline>
            <Input style={styles.input}
              placeholder={placeholderText}
              placeholderTextColor="gray"
              onChangeText={(text) => this.setState({nextQuery: text})}
              onSubmitEditing={this._onPress} />
            <Button style={styles.search} transparent small
              onPress={this._onPress}>
              <Icon name="magnifier" type="SimpleLineIcons"/>
            </Button>
          </Item>
          <TextHeader>By author</TextHeader>
          <SearchResultList dataId="fullName" navigation={navigation}
            query={query} type="author" style={styles.space} />
          <TextHeader style={styles.space}>By title</TextHeader>
          <SearchResultList dataId="id" navigation={navigation}
            query={query} type="title" style={styles.space} />
          <TextHeader style={styles.space}>By keyword</TextHeader>
          <SearchResultList dataId="id" navigation={navigation}
            query={query} type="keyword" style={styles.space} />
          <TextHeader style={styles.space}>By room</TextHeader>
          <SearchResultList dataId="id" navigation={navigation}
            query={query} type="room" style={styles.space} />
        </Content>
      </Container>
    );
  }

  _onPress() {
    const {navigation} = this.props;
    const {nextQuery} = this.state;
    console.log('SearchResultScreen#_onPress()',
        'Given query', nextQuery);

    if (nextQuery.length >= 2) {
      navigation.push('SearchResultScreen', {query: nextQuery});
    } else {
      Toast.show({
        // eslint-disable-next-line
        text: 'Length of search query must longer than or equal to 2 characters',
        buttonText: 'Dismiss',
        position: 'top',
      });
    }
  }
}

SearchResultScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
  placeholderText: PropTypes.string,
};

SearchResultScreen.defaultProps = {
  placeholderText: 'Search by author, paper, keywords, or room',
};

export default SearchResultScreen;
