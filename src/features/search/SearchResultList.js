'use strict';
import NetInfo from '@react-native-community/netinfo';
import {Spinner, Text} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {FlatList, View} from 'react-native';
import {ListItem} from '../../components';
import {getData, Status} from '../../utils';
import styles from './styles';

// TODO Render warning message when search results is empty
class SearchResultList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status: props.data ? Status.SUCCESS : Status.OFFLINE,
      data: props.data ? props.data : [],
    };
  }
  componentDidMount() {
    const {endpoint, query, type} = this.props;
    if (this.state.status !== Status.SUCCESS) {
      NetInfo.isConnected.fetch().then((isConnected) => {
        if (isConnected) {
          this.setState({status: Status.LOADING});
          let params = {};
          switch (type) {
            case 'author':
              params = {author: query};
              break;
            case 'room':
              params = {room: query};
              break;
            case 'title':
              params = {title: query};
              break;
            case 'keyword':
              params = {keyword: query};
              break;
          }
          getData(endpoint, params).then((json) => {
            console.log(`# of ${type} search results: ${json.data.length}`);
            this.setState({
              data: json.data,
              status: Status.SUCCESS,
            });
          }).catch((error) => {
            console.error(error);
            this.setState({status: Status.ERROR});
          });
        }
      });
    }
  }

  componentWillUnmount() {
    // TODO Store events data into local cache
    this.setState({data: []});
  }

  render() {
    if (this.state.status == Status.LOADING) {
      return <Spinner />;
    } else if (this.state.status == Status.SUCCESS) {
      return this.renderSearchResultList();
    } else if (this.state.status == Status.OFFLINE) {
      return <Text>You are offline!</Text>;
    } else {
      return <Text>An error has occurred!</Text>;
    }
  }

  renderSearchResultList() {
    const {dataId, navigation, type, query} = this.props;
    return <FlatList data={this.state.data}
      keyExtractor={(item, _) => item[dataId]}
      ListEmptyComponent={
        <Text style={styles.noMatch}>No match found for {`'${query}'`}</Text>
      }
      ItemSeparatorComponent={() => <View style={styles.separator}></View>}
      renderItem={({item}) =>
        <ListItem onPress={createSpecificOnPress(type, navigation, item)}>
          {createSpecificListItem(type, item)}
        </ListItem>
      }
    />;
  }
}

const createSpecificOnPress = (type, navigation, item) => {
  switch (type) {
    case 'author':
      return () => navigation.push('AuthorSearchResultScreen', {author: item,
        fromSearch: true});
    case 'room':
    case 'title':
    case 'keyword':
      return () => navigation.push('SubmissionDetailScreen',
          {submission: createSubmission(item), fromSearch: true});
    default:
      console.error('Unrecognised data type:', type);
  }
};

// TODO Refactor to utility module
const createSubmission = (item) => {
  const submissionObject = {
    title: item.title,
    date: item.date,
    time: item.time,
    location: item.location,
    chair: item.chair,
    authors: item.authors,
    abstract: item.abstract,
  };

  if (!item.date) submissionObject.date = '';
  if (!item.time) submissionObject.time = '';
  if (!item.location) submissionObject.location = '';
  if (!item.chair) submissionObject.chair = '';

  return submissionObject;
};


const createSpecificListItem = (type, item) => {
  switch (type) {
    case 'author':
      return (
        <View>
          <Text style={styles.name}>{item.fullName}</Text>
          <Text style={styles.itemStyle}>{item.organization}</Text>
          <Text style={styles.itemStyle}>{item.country}</Text>
        </View>
      );
    case 'room':
    case 'title':
    case 'keyword':
      return (
        <View>
          <Text style={styles.track}>{findSymposiumName(item.track)}</Text>
          <Text style={styles.name}>{item.title}</Text>
          <Text style={styles.author}>{item.authors}</Text>
        </View>
      );
  }
};

const findSymposiumName = (abbreviation) => {
  switch (abbreviation) {
    case 'SGR':
      return 'Smart Grid and Regulation';
    case 'BIO':
      return 'Bioenergy';
    case 'FC':
      return 'Fuel Cell Technology';
    case 'ES':
      return 'Energy Storage';
    case 'MAM':
      return 'Multifunctional and Advanced Materials';
    case 'ETBE':
      return 'Eco Tropical Built Environment';
  }
};

SearchResultList.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  dataId: PropTypes.string.isRequired,
  endpoint: PropTypes.string,
  navigation: PropTypes.object.isRequired,
  query: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};

SearchResultList.defaultProps = {
  endpoint: 'https://asia-northeast1-i-trec.cloudfunctions.net/search',
};

export default SearchResultList;
