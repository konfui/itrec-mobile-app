'use strict';
import {Dimensions, StyleSheet} from 'react-native';
import qir from '../../../native-base-theme/variables/qir';

const dimensions = Dimensions.get('window');
const imageHeight = Math.round(dimensions.width * 9 / 24);
const imageWidth = dimensions.width;

export default StyleSheet.create({
  header: {
    backgroundColor: qir.brandPrimary,
  },
  container: {
    backgroundColor: qir.brandPrimary,
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 8,
  },
  // eslint-disable-next-line react-native/no-color-literals
  title: {
    color: 'white',
    fontWeight: 'bold',
    marginHorizontal: 2,
    textTransform: 'uppercase',
  },
  image: {
    alignSelf: 'flex-start',
    padding: 1,
    width: imageWidth,
    height: imageHeight,
  },
});

