'use strict';
import {Body, Card, CardItem, Container, Content} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {Image, View} from 'react-native';
import {HeaderBurger, withFirebaseImage} from '../../components';
import styles from './styles';

// %TODO% filenames
const SponsorScreen = (props) => {
  const CohostedImage = withFirebaseImage(Image, '/sponsor/cohosted.png');
  const SilverImage = withFirebaseImage(Image, '/sponsor/silver.png');
  const BronzeImage = withFirebaseImage(Image, '/sponsor/bronze.png');
  const SupportedImage = withFirebaseImage(Image, '/sponsor/supportedby.png');
  const {navigation} = props;

  return (
    <Container>
      <Content>
        <HeaderBurger title='Sponsors' navigation={navigation}/>
        <View>
          <Card>
            <CardItem>
              <Body>
                <CohostedImage resizeMode='contain' style={styles.image} />
              </Body>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <Body>
                <SilverImage resizeMode='contain' style={styles.image} />
              </Body>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <Body>
                <BronzeImage resizeMode='contain' style={styles.image} />
              </Body>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <Body>
                <SupportedImage resizeMode='contain' style={styles.image} />
              </Body>
            </CardItem>
          </Card>
        </View>
      </Content>
    </Container>
  );
};

SponsorScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default SponsorScreen;
