'use strict';
import {StyleSheet} from 'react-native';
import qir from '../../../native-base-theme/variables/qir';

export default StyleSheet.create({
  title: {
    color: qir.brandPrimary,
    fontWeight: 'bold',
    marginHorizontal: 2,
    textTransform: 'uppercase',
  },
  itemStyle: {
    color: qir.brandGrey,
    fontSize: 14,
    padding: 8,
    textAlign: 'justify',
  },
});

