'use strict';
import React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import NewsDetailScreen from '../NewsDetailScreen';

describe('A news detail component', () => {
  let mockNavigation = null;

  beforeEach(() => {
    mockNavigation = {
      getParam: jest.fn((param, defaultParam) => {
        return 'http://localhost';
      }),
    };
  });

  it('matches its snapshot correctly', () => {
    const component = TestRenderer.create(
        <NewsDetailScreen navigation={mockNavigation} />
    );

    expect(component.toJSON()).toMatchSnapshot();
  });
});
