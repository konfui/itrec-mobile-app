'use strict';
import React from 'react';
import {Card, CardItem, Text, Icon} from 'native-base';
import {TouchableHighlight, View} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import qir from '../../../native-base-theme/variables/qir';

const SymposiumCard = ({title, onPress}) =>
  <TouchableHighlight underlayColor={qir.brandLighter}
    onPress={onPress}>
    <Card style={styles.container}>
      <CardItem style={styles.content} header>
        <View style={styles.leftColumn}>
          <Text style={styles.mainTitle}>{title}</Text>
        </View>
        <View style={styles.rightColumn}>
          <Icon style={styles.iconStyle} type="SimpleLineIcons"
            name='arrow-right' />
        </View>
      </CardItem>
    </Card>
  </TouchableHighlight>;

SymposiumCard.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default SymposiumCard;
