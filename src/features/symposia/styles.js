'use strict';
import {StyleSheet} from 'react-native';
import qir from '../../../native-base-theme/variables/qir';

export default StyleSheet.create({
  activeTab: {
    backgroundColor: '#5e3876', // %TODO%
  },
  activeTabText: {
    color: 'white',
    textTransform: 'uppercase',
  },
  container: {
    backgroundColor: qir.brandPrimary,
  },
  content: {
    backgroundColor: qir.brandPrimary,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  header: {
    backgroundColor: qir.brandPrimary,
  },
  iconStyle: {
    color: 'white',
    fontSize: 16,
  },
  leftColumn: {
    flex: 3,
    flexDirection: 'column',
  },
  mainTitle: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  rightColumn: {
    alignItems: 'flex-end',
    flex: 1,
    marginEnd: 16,
  },
  sessionTitle: {
    color: 'white',
  },
  tab: {
    backgroundColor: '#f5f5f5', // %TODO%
  },
  tabText: {
    color: '#4c4c4c',
    textTransform: 'uppercase',
  },
  input: {
    fontSize: 14,
  },
  search: {
    alignSelf: 'center',
  },
});
