'use strict';
import React from 'react';
import {Button, Container, Content, Header, Icon,
  Input, Item, Toast} from 'native-base';
import PropTypes from 'prop-types';
import {TextHeader2} from '../../components';
import SymposiaListContainer from './SymposiaListContainer';
import styles from './styles';

class SymposiaListScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {query: ''};
    this._onPress = this._onPress.bind(this);
  }

  render() {
    // %TODO% first & second props
    const {placeholderText} = this.props;
    return (
      <Container>
        <Header hasTabs style={styles.header}>
          <TextHeader2 first="Parallel" second="Sessions" />
        </Header>
        <Content>
          <Item underline>
            <Input style={styles.input}
              placeholder={placeholderText}
              placeholderTextColor="gray"
              onChangeText={(text) => this.setState({query: text})}
              onSubmitEditing={this._onPress} />
            <Button style={styles.search} transparent small
              onPress={this._onPress}>
              <Icon name="magnifier" type="SimpleLineIcons"/>
            </Button>
          </Item>
          <SymposiaListContainer navigation={this.props.navigation} />
        </Content>
      </Container>
    );
  }

  _onPress() {
    const {navigation} = this.props;
    const {query} = this.state;
    console.log('SymposiaListScreen#_onPress()',
        'Given query', this.state.query);

    if (query.length >= 2) {
      navigation.push('SearchResultScreen', {query: query});
    } else {
      Toast.show({
        // eslint-disable-next-line
        text: 'Length of search query must longer than or equal to 2 characters',
        buttonText: 'Dismiss',
        position: 'top',
      });
    }
  }
}

SymposiaListScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
  placeholderText: PropTypes.string,
};

SymposiaListScreen.defaultProps = {
  placeholderText: 'Search by author, paper, keywords, or room',
};

export default SymposiaListScreen;
