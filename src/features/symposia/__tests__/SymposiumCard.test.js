'use strict';
import React from 'react';
import 'react-native';
import {Icon} from 'native-base';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import {shallow} from 'enzyme';
import SymposiumCard from '../SymposiumCard';

describe('A symposium card component', () => {
  const FAKE_TITLE = 'International Symposium on Test';
  let mockNavigation = {};

  beforeEach(() => {
    mockNavigation = {push: jest.fn()};
  });

  it('matches its snapshot correctly', () => {
    const component = TestRenderer.create(
        <SymposiumCard title={FAKE_TITLE}
          onPress={mockNavigation.push} />
    );

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('has a single arrow icon', () => {
    const wrapper = shallow(
        <SymposiumCard title={FAKE_TITLE}
          onPress={mockNavigation.push} />
    );

    const icons = wrapper.find(Icon);

    expect(icons.length).toEqual(1);
    expect(icons.first().prop('name')).toBe('arrow-right');
  });

  it('pressable', () => {
    const wrapper = shallow(
        <SymposiumCard title={FAKE_TITLE}
          onPress={mockNavigation.push} />
    );

    wrapper.simulate('press');

    expect(mockNavigation.push).toBeCalled();
  });
});
