'use strict';
import PlenaryListScreen from './PlenaryListScreen';
import PlenaryDetailScreen from './PlenaryDetailScreen';

export {PlenaryListScreen, PlenaryDetailScreen};
