'use strict';
import {Container, Content, Header} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {TextHeader2} from '../../components';
import PlenaryListContainer from './PlenaryListContainer';
import styles from './styles';

// %TODO% first & second props
const PlenaryListScreen = ({data, navigation}) =>
  <Container>
    <Header style={styles.header}>
      <TextHeader2 first="Keynote/Invited" second="Speakers" />
    </Header>
    <Content>
      <PlenaryListContainer data={data} navigation={navigation} />
    </Content>
  </Container>;

PlenaryListScreen.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  navigation: PropTypes.object.isRequired,
};

export default PlenaryListScreen;
