// SPDX-License-Identifier: GPL-3.0-or-later
'use strict';
import createPhotoFilename from '../createPhotoFilename';
describe('createPhotoFilename()', () => {
  it('handles name with title and period', () => {
    expect(createPhotoFilename('Prof. Giorno Giovanna', 'jpg'))
        .toEqual('prof_giorno_giovanna.jpg');
  });

  it('handles name with parentheses', () => {
    expect(createPhotoFilename('Prof. dr. (Ricardo) Milos', 'png'))
        .toEqual('prof_dr_ricardo_milos.png');
  });

  it('handles name with dash', () => {
    expect(createPhotoFilename('Dr. Wu-Tang Zhou', 'jpg'))
        .toEqual('dr_wu-tang_zhou.jpg');
  });

  it('handles name with comma', () => {
    expect(createPhotoFilename('Prof. Joseph Joestar, PhD', 'png'))
        .toEqual('prof_joseph_joestar_phd.png');
  });

  it('produces filename without excessive whitespaces', () => {
    expect(createPhotoFilename('Dr.  Guido Mista,   PhD   ', 'jpg'))
        .toEqual('dr_guido_mista_phd.jpg');
  });

  it('produces filename with all lowercase characters', () => {
    expect(createPhotoFilename('PROF. DIAvolo', 'png'))
        .toEqual('prof_diavolo.png');
  });
});
