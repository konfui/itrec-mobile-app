'use strict';
import React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import HeaderImage from '../HeaderImage';
import assets from '../../assets';

describe('A header image component', () => {
  it('matches its snapshot correctly in landscape orientation', () => {
    jest.mock('Dimensions', () => {
      return {
        get: (window) => {
          return {
            width: 1280,
            height: 720,
          };
        },
      };
    });
    const component = TestRenderer.create(
        <HeaderImage source={assets.header_logo} />
    );

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('matches its snapshot correctly in portrait orientation', () => {
    jest.mock('Dimensions', () => {
      return {
        get: (window) => {
          return {
            width: 720,
            height: 1280,
          };
        },
      };
    });
    const component = TestRenderer.create(
        <HeaderImage source={assets.header_logo} />
    );

    expect(component.toJSON()).toMatchSnapshot();
  });
});
