// SPDX-License-Identifier: GPL-3.0-or-later
'use strict';
import PropTypes from 'prop-types';
import React from 'react';
import {Dimensions, Image, StyleSheet} from 'react-native';

const dimensions = Dimensions.get('window');
const imageHeight = Math.round((dimensions.width * 9) / 16);
const imageWidth = dimensions.width;

const HeaderImage = ({source}) =>
  <Image
    resizeMode='stretch'
    source={source}
    style={styles.landscapeLogo} />;

HeaderImage.propTypes = {
  source: PropTypes.oneOfType([
    PropTypes.object, // Data type when running unit testing
    PropTypes.number, // Data type when running on device
  ]).isRequired,
};

const styles = StyleSheet.create({
  landscapeLogo: {
    height: imageHeight,
    width: imageWidth,
  },
});

export default HeaderImage;
