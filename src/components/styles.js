'use strict';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 8,
  },
  iconStyle: {
    fontSize: 14,
  },
  leftColumn: {
    flex: 3,
    flexDirection: 'column',
  },
  rightColumn: {
    alignItems: 'flex-end',
    flex: 1,
    marginEnd: 16,
  },
  search: {
    alignSelf: 'center',
  },
});
