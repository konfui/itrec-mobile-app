#!/usr/bin/env bash
# %TODO%
if [[ -z "$GCP_BUCKET_NAME" ]]; then
    echo "Using default (qir-ftui-cicd) as bucket name"
    GCP_BUCKET_NAME="qir-ftui-cicd"
else
    echo "Using $GCP_BUCKET_NAME as bucket name"
    GCP_BUCKET_NAME=$GCP_BUCKET_NAME
fi

for APK in $(ls -1 *.apk);
do
    node ci/save_to_cloud_storage.js $GCP_BUCKET_NAME $APK
done
exit 0
