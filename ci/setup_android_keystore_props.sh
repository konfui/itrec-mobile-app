#!/usr/bin/env bash

# Exit immediately on any failure
set -e
set -o pipefail

echo ""
echo "Creating properties file for loading the keystore during Android build process"
echo ""

printf "Checking required arguments..."
KEYSTORE_PASS="$1"
KEYSTORE_ALIAS="$2"
KEYSTORE_PATH="$3"
echo "OK"

mkdir -p ../android/keystores
echo "storePassword=$KEYSTORE_PASS" >> ../android/keystores/release.keystore.properties
echo "keyPassword=$KEYSTORE_PASS" >> ../android/keystores/release.keystore.properties
echo "keyAlias=$KEYSTORE_ALIAS" >> ../android/keystores/release.keystore.properties
echo "storeFile=$KEYSTORE_PATH" >> ../android/keystores/release.keystore.properties

echo ""
echo "Created properties file at ../android/keystores/release.keystore.properties"
echo ""

exit 0