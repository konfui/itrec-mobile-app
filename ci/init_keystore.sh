#!/usr/bin/env bash

# Exit immediately on any failure
set -e
set -o pipefail

echo ""
echo "Initialising keystore"
echo ""

printf "Checking whether keytool is installed..."
if [[ ! -x "$(command -v keytool)" ]]; then
    echo "Error: keytool is not installed." >&2
    echo "Make sure you have installed OpenJDK."
    exit 1
fi
echo "OK"

printf "Checking required arguments..."
KEYSTORE_FILE="$1"
KEYSTORE_PASS="$2"
RELEASE_ALIAS="$3"
echo "OK"

printf "Creating keystore for release variant..."
keytool -genkeypair -alias "$RELEASE_ALIAS" -keyalg RSA -keysize 4096 \
        -storepass "$KEYSTORE_PASS" -keystore ~/$KEYSTORE_FILE \
        -validity 12000
echo "OK"

echo ""
echo "Created keystore at ~/$KEYSTORE_FILE"
echo ""

exit 0