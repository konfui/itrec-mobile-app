#!/usr/bin/env bash

# Exit immediately on any failure
set -e
set -o pipefail

KEYSTORE_PATH="$1"
if [[ ! -f "$KEYSTORE_PATH" ]]; then
    echo "Error: Keystore file not found at $KEYSTORE_PATH." >&2
    exit 1
fi

cat $KEYSTORE_PATH | base64 -w0

exit 0