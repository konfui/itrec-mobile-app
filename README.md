# Konfui - iTrec Mobile _(konfui-itrec-mobile)_

[![pipeline status](https://gitlab.com/konfui/qir-mobile/badges/master/pipeline.svg)](https://gitlab.com/konfui/qir-mobile/commits/master)
[![coverage report](https://gitlab.com/konfui/qir-mobile/badges/master/coverage.svg)](https://gitlab.com/konfui/qir-mobile/commits/master)

> Mobile app variant based on Konfui core for iTrec conference at the Faculty
> of Engineering Universitas Indonesia. Developed at Pusilkom UI.

Konfui is a platform for rapidly building mobile app products that serve
information from an ongoing academic conference. It is designed as a software
product line that allows developers to reuse existing artefacts (e.g. source
code, images, styles) and provide custom, conference-specific implementation
when building a new product variant.

At its current state, Konfui can only produce new product variant by following
_clone-and-own_ approach that still involves manual steps. The maintainers aim
to introduce semi-automated approach by providing a CLI tool for generating new
product variants in the future.

## Prerequisites

This project uses React and React Native as the core technologies for building
the mobile apps. Consequently, the following dependencies must be installed
prior to developing and building the apps:

- [Node.js v8.15+](https://nodejs.org/en/download/releases/)
  > Note: We recommend `nvm` (Node Version Manager) for installing
  > Node.js. You can find `nvm` for your OS at the following links:
  > - [nvm for macOS or GNU/Linux-based OS](https://github.com/creationix/nvm)
  > - [nvm for Windows](https://github.com/coreybutler/nvm-windows)
  >
  > Once `nvm` has been installed, install Node.js and activate it
  > by executing `nvm install 8.16.0` followed by `nvm activate 8.16.0`.
- [`yarn` package manager](https://yarnpkg.com/en/docs/install)
  > Note: You can install `yarn` using `npm` package manager provided by
  > standard Node.js installation. The installation command is
  > `npm install -g yarn`.

Verify that all basic dependencies have been installed successfully. Make sure
`nvm`, `node`, and `yarn` can be invoked from within the shell. For example,
in `bash` shell (Mac or GNU/Linux-based OS):

```bash
$ nvm version
1.1.7
$ node --version
v8.16.0
$ yarn --version
1.15.2
```

> Note: The example for Command Prompt or PowerShell on Windows should look
> similar to the example above.

You also need to install the dependencies for building on each target platforms
(Android and iOS). For Android, you need:

- [Java OpenJDK v8+ with HotSpot JVM](https://adoptopenjdk.net/)
  > Note: Please make sure `JAVA_HOME` environment variable is properly set in
  > the local development machine. `JAVA_HOME` points to the directory where
  > Java is installed. You also need to ensure that `java` and `javac` can be
  > called from within the shell.
- (Required on Windows) [Python v2.7+](https://www.python.org/downloads/)

For iOS, make sure you have an actual iMac or Macbook. You need:

- Minimum macOS version: [macOS High Sierra](https://en.wikipedia.org/wiki/MacOS_High_Sierra)
- Xcode v9.4+

Once all basic dependencies are ready, proceed to clone the codebase and
install the required Node.js packages for developing and building the app.

- macOS or GNU/Linux-based OS

  ```bash
  # Assuming current working directory is at /home/MyName/
  git clone https://gitlab.com/konfui/qir-mobile.git ~/qir-mobile
  cd ~/qir-mobile
  yarn global add react-native-cli@2.0.1
  yarn install
  ```

- Windows

  ```batch
  :: Assuming current working directory is at C:\Users\MyName\
  git clone https://gitlab.com/konfui/qir-mobile.git qir-mobile
  cd qir-mobile
  yarn global add react-native-cli@2.0.1
  yarn install
  ```

Run the unit tests suite to make sure the codebase can be tested in local
development environment.

```bash
yarn test
```

Finally, please follow the [Getting Started](https://facebook.github.io/react-native/docs/getting-started)
guide on official React Native documentation site. Choose the instructions for
**React Native CLI Quickstart**. Then, read the instructions that match your
development and target OS platform as illustrated in the following animation:

![Platform-specific instructions for setting up React Native](docs/images/rn_getting_started.gif)

## Build

For the first time (or whenever someone added new `npm` packages):

```bash
$ yarn install --dev
$ yarn run jetify
```

> Note: When you begin to develop a new product variant, you need to generate a
> new `google-services.json` from your app's Firebase dashboard and replace the
> default `google-services.json` in the codebase.

### Android

There are three build variants on Android. The first variant, `debug`, is the
app itself without any JavaScript assets bundled. The second variant, `release`
is the app that ready for publishing on Google Play Store. The third variant,
`review` is a `debug` variant but bundled with JavaScript assets, thus allows
the app run standalone in the device.

The Android app build process requires a Java Keystore (JKS) containing the
public-private keys for signing the `review` and `release` variants. Please
contact the maintainer to obtain the JKS file or create a new one if building
the app for different conference/event.

Before building a build variant, prepare the properties for authenticating
to the JKS in the Android source set beforehand:

```bash
cd android/keystores
touch review.keystore.properties
echo storePassword=KEYSTORE_PASS >> review.keystore.properties
echo keyPassword=KEYSTORE_PASS >> review.keystore.properties
echo keyAlias=qir-review >> review.keystore.properties
echo storeFile=KEYSTORE_LOCATION >> review.keystore.properties
touch release.keystore.properties
echo storePassword=KEYSTORE_PASS >> release.keystore.properties
echo keyPassword=KEYSTORE_PASS >> release.keystore.properties
echo keyAlias=qir-release >> release.keystore.properties
echo storeFile=KEYSTORE_LOCATION >> release.keystore.properties
```

> Note: Ask the maintainer(s) to obtain the actual value for `KEYSTORE_PASS`

To generate a build variant, e.g. `review`, invoke the corresponding `assemble`
Gradle task. Example:

```bash
cd android
./gradlew assembleReview
cd app/build/outputs/apk/review
adb install app-review.apk
```

### iOS

To run the app in the simulator (e.g. iPhone 6):

```bash
$ react-native run-ios --simulator "iPhone 6"
```

## Clone

1. Create a Java Keystore and store it in a quick-to-find location, e.g.
   at home directory.
   > Note: Use [init_keystore.sh](ci/init_keystore.sh) helper script
   > to initialise a new Java Keystore for the first time. The generated
   > keystore will be written into the home directory. Example:
   >
   > ```bash
   > # Example for macOS or GNU/Linux-based OS
   > chmod +x ci/init_keystore.sh
   > ./ci/init_keystore.sh itrec-mobile.keystore YOUR_PASSWORD itrec-release
   > ```

1. Create a Java properties file for configuring the build process to load
   the keystore during compilation.
   > Note: Use [setup_android_keystore_props.sh](ci/setup_android_keystore_props.sh)
   > helper script to create the properties file. The script will create
   > a new folder named `keystores` inside `android` directory and create
   > a single properties file in it.

1. Update application name manually across React Native, Android, and iOS
   codebase. The following is the list of sections that need to be updated:
   - JavaScript module name, i.e. `name` property in
     [`package.json`](package.json). Example:

     ```javascript
     {
         // Original:
         "name": "@pusilkomui/qir-mobile",
         ...
         // Updated:
         "name": "@pusilkomui/itrec-mobile",
         ...
     }
     ```

   - Android root project name in [`settings.gradle`](android/settings.gradle).
     Example:

     ```gradle
     // Original:
     rootProject.name = 'QIRMobile'
     // Updated:
     rootProject.name = 'ITRECMobile'
     ```

   - Package name for two main Java (Android) classes, `MainActivity` and
     `MainApplication`, and ensure the location follows the package name
     correctly. First, find both classes using Android studio and perform
     **Move** refactoring action, as depicted in the following animation:

     ![Performing **Move** refactoring](docs/images/android_move.gif)
     Subsequently, rename the package name in `MainActivity` and
     `MainApplication` Java source code files to follow the location of
     both files, as depicted in the following animation:

     ![Renaming classess](docs/images/android_rename.gif)
     Finally, update package name in [`AndroidManifest.xml`](android/app/src/main/AndroidManifest.xml).

## Maintainers & Contributors

- [Daya Adianto](https://gitlab.com/addianto)
- [Danny August](https://gitlab.com/daystram)
- [Ganda Fitrandia](https://gitlab.com/ganfitran)
- [Meitya Dianti](https://gitlab.com/meityad)
- [Ade Azurat](https://gitlab.com/adeazurat)

## License

Copyright (C) 2019 Daya Adianto. Developed at [Pusilkom UI](http://pusilkom.ui.ac.id)
for [iTrec Conference](https://i-trec.ui.ac.id) organised by the
[Faculty of Engineering Universitas Indonesia](http://eng.ui.ac.id).

This project is licensed under [GPLv3](LICENSE). The initial codebase is based
on [ICAS-PGS](https://gitlab.com/addianto/icas-pgs) app that licensed under
[Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0.html).
